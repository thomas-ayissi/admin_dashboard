<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;
class ProductController extends Controller
{
    
    public function fetchCategory( Request $request){
        if ($request->ajax()) {
            $data = Category::select('*');
            $id = Category::select('category_id');
            return Datatables::of($data,$id)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                           $btn = '<button id="edit" data-id='.$row->category_id.' type="button" class="btn btn-sm btn-outline-warning"><i class="fa fa-edit fa-fw"></i>Edit</button>';
                           $btn.='<button id="delete" data-id='.$row->category_id.' type="button" class="btn btn-sm btn-outline-danger"><i class="fa fa-trash fa-fw"></i>Delete</button>';
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        return view('category');
    }
    public function listView(){
        $products = Product::all();
        return view('admin.icons',['products' => $products]);
    }
    public function storeCategory(Request $request){
        if(empty($request->category_name) && empty($request->category_description)){
            return response()->json([
                'status' => 'empty',
                'message' => 'Field is required!'
            ]);
        }elseif(empty($request->category_name)){
            return response()->json([
                'status' => 'empty_name',
                'message' => 'Category Name is required!'
            ]);
        }
        elseif(empty($request->category_description)){
            return response()->json([
                'status' => 'empty_des',
                'message' => 'Category Description is required!'
            ]);
        }elseif(strlen($request->category_name) < 3 && strlen($request->category_description) < 3 || strlen($request->category_name) >50 && strlen($request->category_description) > 50){
            return response()->json([
                'status' => 'min_max',
                'message' => 'Field must be least than 50 and more than 3 charaters'
            ]);
        }elseif(strlen($request->category_name) > 50 || strlen($request->category_name) < 3){
            return response()->json([
                'status' => 'min_max_name',
                'message' => 'Category Name must be least than 50 and more than 3 charaters'
            ]);
        }elseif(strlen($request->category_description) > 50 || strlen($request->category_description) < 3){
            return response()->json([
                'status' => 'min_max_des',
                'message' => 'Category Description must be least than 50 and more than 3 charaters'
            ]);

        }else{
            $category = new Category;
            $category->category_name = $request->input('category_name');
            $category->category_description = $request->input('category_description');
            $category->save();
            return response()->json([
                'status' => 'success',
                'message' => 'Successfully Add Category'
            ]);
        }
    }
    public function getDelete($id){
        $category = Category::where('category_id',$id)->get();
        return response()->json([
            'status' => 'success',
            'category' => $category
        ]);
    }
    public function destroyCategory($id){
        $category = Category::where('category_id',$id);
        $category_name = Category::where('category_id',$id)->get();
        if($category){
            $category->delete();
            return response()->json(['status'=>'success','message'=>'Successfully Delete!']);
            
        }
    }

    public function getEditCategory($id){
        $category = Category::where('category_id',$id)->get();
        return response()->json([
            'status' => 'success',
            'category' => $category
        ]);
    }
    public function updateCategory(Request $request,$id){
        if(empty($request->category_name) && empty($request->category_description)){
            return response()->json([
                'status' => 'empty',
                'message' => 'Field is required!'
            ]);
        }elseif(empty($request->category_name)){
            return response()->json([
                'status' => 'empty_name',
                'message' => 'Category Name is required!'
            ]);
        }
        elseif(empty($request->category_description)){
            return response()->json([
                'status' => 'empty_des',
                'message' => 'Category Description is required!'
            ]);
        }elseif(strlen($request->category_name) < 3 && strlen($request->category_description) < 3 || strlen($request->category_name) >50 && strlen($request->category_description) > 50){
            return response()->json([
                'status' => 'min_max',
                'message' => 'Field must be least than 50 and more than 3 charaters'
            ]);
        }elseif(strlen($request->category_name) > 50 || strlen($request->category_name) < 3){
            return response()->json([
                'status' => 'min_max_name',
                'message' => 'Category Name must be least than 50 and more than 3 charaters'
            ]);
        }elseif(strlen($request->category_description) > 50 || strlen($request->category_description) < 3){
            return response()->json([
                'status' => 'min_max_des',
                'message' => 'Category Description must be least than 50 and more than 3 charaters'
            ]);

        }else{
            $category = Category::where('id',$id);
            $category->category_name = $request->input('category_name');
            $category->category_description = $request->input('category_description');
            $category->update();
            return response()->json([
                'status' => 'success',
                'message' => 'Successfully Add Category'
            ]);
        }
    }
}

