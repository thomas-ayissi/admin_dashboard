<?php

use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Get View
Route::get('/', function () {
    return view('admin.home');
})->name('home');
// Route::get('icons', function () {
//     return view('admin.icons');
// })->name('admin.icons');
Route::get('category', function () {
    return view('admin.category');
})->name('admin.category');

Route::get('profile', function () {
    return view('admin.profile');
})->name('admin.profile');
Route::get('tables', function () {
    return view('admin.tables');
})->name('admin.tables');
Route::get('login', function () {
    return view('admin.login');
})->name('admin.login');
Route::get('register', function () {
    return view('admin.register');
})->name('admin.register');
Route::get('upgrade', function () {
    return view('admin.upgrade');
})->name('admin.upgrade');

Route::get('icons',[ProductController::class,'listView'])->name('admin.icons');
// Route::get('maps',[ProductController::class,'getCategory'])->name('admin.maps');
Route::get('fetch.category',[ProductController::class,'fetchCategory'])->name('fetch.category');
Route::post('add.category',[ProductController::class,'storeCategory'])->name('add.category');
Route::get('get.delete/{id}',[ProductController::class,'getDelete'])->name('get.delete');
Route::delete('delete.category/{id}',[ProductController::class,'destroyCategory'])->name('destroy.category');
Route::get('get_edit.category/{id}',[ProductController::class,'getEditCategory'])->name('getEdit.category');
Route::get('update.category/{id}',[ProductController::class,'updateCategory'])->name('update.category');